/*
 * Copyright 2012-2016 Freescale Semiconductor, Inc.
 * Copyright 2011 Linaro Ltd.
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>

/ {

	chosen {
		stdout-path = &uart2;
	};

	

	memory: memory {
		reg = <0x10000000 0x80000000>;
	};


};

&clks {
	assigned-clocks = <&clks IMX6QDL_CLK_LDB_DI0_SEL>,
			  <&clks IMX6QDL_CLK_LDB_DI1_SEL>;
	assigned-clock-parents = <&clks IMX6QDL_CLK_PLL2_PFD0_352M>,
				 <&clks IMX6QDL_CLK_PLL2_PFD0_352M>;
};

&clks {
};


&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c3>;
	status = "okay";

	pmic: pfuze100@08 {
	compatible = "fsl,pfuze100";
	reg = <0x08>;

	regulators {
		sw1a_reg: sw1ab {
			regulator-min-microvolt = <300000>;
			regulator-max-microvolt = <1875000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-ramp-delay = <6250>;
		};

		sw1c_reg: sw1c {
			regulator-min-microvolt = <300000>;
			regulator-max-microvolt = <1875000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-ramp-delay = <6250>;
		};

		sw2_reg: sw2 {
			regulator-min-microvolt = <800000>;
			regulator-max-microvolt = <3300000>;
			regulator-boot-on;
			regulator-always-on;
			regulator-ramp-delay = <6250>;
		};

		sw3a_reg: sw3a {
			regulator-min-microvolt = <400000>;
			regulator-max-microvolt = <1975000>;
			regulator-boot-on;
			regulator-always-on;
		};

		sw3b_reg: sw3b {
			regulator-min-microvolt = <400000>;
			regulator-max-microvolt = <1975000>;
			regulator-boot-on;
			regulator-always-on;
		};

		sw4_reg: sw4 {
			regulator-min-microvolt = <800000>;
			regulator-max-microvolt = <3300000>;
		};

		swbst_reg: swbst {
			regulator-min-microvolt = <5000000>;
			regulator-max-microvolt = <5150000>;
		};

		snvs_reg: vsnvs {
			regulator-min-microvolt = <1000000>;
			regulator-max-microvolt = <3000000>;
			regulator-boot-on;
			regulator-always-on;
		};

		vref_reg: vrefddr {
			regulator-boot-on;
			regulator-always-on;
		};

		vgen1_reg: vgen1 {
			regulator-min-microvolt = <800000>;
			regulator-max-microvolt = <1550000>;
		};

		vgen2_reg: vgen2 {
			regulator-min-microvolt = <800000>;
			regulator-max-microvolt = <1550000>;
		};

		vgen3_reg: vgen3 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
		};

		vgen4_reg: vgen4 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};

		vgen5_reg: vgen5 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};

		vgen6_reg: vgen6 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};
	};
};	
	
};

&iomuxc {
	pinctrl-names = "default";

	imx6qdl-sabresd {
		
		pinctrl_hog: hoggrp {
			fsl,pins = <
				MX6QDL_PAD_SD3_DAT7__GPIO6_IO17	0x80000000
			>;
		};
		
		pinctrl_i2c3: i2c3grp {
			fsl,pins = <
				MX6QDL_PAD_GPIO_5__I2C3_SCL		0x4001b8b1
				MX6QDL_PAD_GPIO_6__I2C3_SDA		0x4001b8b1
			>;
		};

		
		/* UART2 - TODO */
		pinctrl_uart2: uart2grp {
			fsl,pins = <
				
				MX6QDL_PAD_EIM_D26__UART2_TX_DATA	0x1b0b1
				MX6QDL_PAD_EIM_D27__UART2_RX_DATA	0x1b0b1
			>;
		};



		pinctrl_usdhc3: usdhc3grp {
			fsl,pins = <
				MX6QDL_PAD_SD3_CMD__SD3_CMD		0x17059
				MX6QDL_PAD_SD3_CLK__SD3_CLK		0x10059
				MX6QDL_PAD_SD3_DAT0__SD3_DATA0		0x17059
				MX6QDL_PAD_SD3_DAT1__SD3_DATA1		0x17059
				MX6QDL_PAD_SD3_DAT2__SD3_DATA2		0x17059
				MX6QDL_PAD_SD3_DAT3__SD3_DATA3		0x17059
			>;
		};


	};
};


&reg_arm {
       vin-supply = <&sw1a_reg>;
};

&reg_pu {
       vin-supply = <&sw1c_reg>;
};

&reg_soc {
       vin-supply = <&sw1c_reg>;
};

&snvs_poweroff {
	status = "okay";
};


&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};



&usdhc3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usdhc3>;
	bus-width = <4>;
	cd-gpios = <&gpio6 17 GPIO_ACTIVE_LOW>;
	no-1-8-v;
	keep-power-in-suspend;
	enable-sdio-wakeup;
	status = "okay";
};


